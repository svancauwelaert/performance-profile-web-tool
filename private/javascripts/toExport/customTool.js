$(function() {
	setUpProfileTool();
});

function setUpProfileTool() {

	//left column
	var columnLeft = $('<div class="col-sm-2 text-center custom-background"> </div>');

	var headerLeft = $('<h4> Controls </h4>');

	columnLeft.append(headerLeft);
	columnLeft.append('<hr>');

	//baselines
	var baselines = $('\
		<div class="btn-group"> \
	                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \
	                  Baseline(s) <span class="caret"></span> \
	                </button> \
	                <ul id="baselines" class="dropdown-menu"> \
	                </ul> \
	              </div> \
		');

	var infoBaselines = $('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="Baseline selection allows to set baseline set. Only elements of this set will be considered to compute the denominator in the metric ratio. For a given instance, the denominator will be the minimal value of the baseline set elements."></i>');

	// labels
	var labels = $('\
		<div class="btn-group">\
	                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
	                  Label(s) <span class="caret"></span>\
	                </button>\
	                <ul id="labels" class="dropdown-menu">\
	                </ul>\
	              </div>\
		');
	var infoLabels = $('\
		<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="Labels selectors. An instance that is labeled with an unselected label will not be considered in the computed profile."></i>\
		');

	var reductionLabel = $('<label for="reduction-sliders">Scaling factor(s)</label>');

	var infoReduction = $('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="A slider applies a reduction percentage to the corresponding metric component."></i> ')

	var divReduction = $('<div id="reduction-sliders"> </div>');

	var xScaleLabel = $('<label for="xscale-type-checkbox">x scale type</label>');
	var xScale = $('<input type="checkbox" id="xscale-type-checkbox" data-off-text="Log" data-on-text="Linear" checked>');

	var minTauLabel = $('<label for="minTau">&tau;<sub>min</sub> </label>');

	var minTau = $('<input type="number" class="form-control" min="0" step=any name="minTau" id="minTau" value="0"/>');

	var maxTauLabel = $('<label for="maxTau">&tau;<sub>max</sub></label>') ;

	var maxTau = $('<input type="number" class="form-control" min="0" step=any name="maxTau" id="maxTau" value="2"/>');

	var minBaselineLabel = $('<label for="minBaselineMetric">Min. Baseline Metric</label>');

	var infoMinBaseline = $('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="The instance with a baseline time lower than this value will be filtered out before constructing the profiles."></i> ');

	var minBaseline = $('<input type="number" class="form-control" min="0" step=any name="minBaselineMetric" id="minBaselineMetric" value="0"/>');

	var minUnsolvedMetricLabel = $('<label for="minUnsolvedMetric">Min. Unsolved Metric</label>');

	var infoMinUnsolvedMetric = $('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="Value used as a minimum metric to state an instance is not solved. An instance is considered unsolved by an approach if its metric is larger than this value."></i> ');

	var minUnsolvedMetric = $('<input type="number" class="form-control" min="0" step=any name="minUnsolvedMetric" id="minUnsolvedMetric" value="9007199254740991"/>');

	columnLeft.append(baselines,infoBaselines,'<hr>',labels, infoLabels, '<hr>',
		reductionLabel, infoReduction, divReduction, '<hr>',
		xScaleLabel, xScale, '<hr>',
		minTauLabel, minTau, maxTauLabel, maxTau, '<hr>',
		minBaselineLabel, infoMinBaseline, minBaseline, '<hr>',
		minUnsolvedMetricLabel, infoMinUnsolvedMetric, minUnsolvedMetric
		);

	//right column
	var columnRight = $('<div class="col-sm-10" id="tool-columnRight"> </div>');
	var rowRight = $('<div class="row"> </div>');
	var svgChart = $('<div id="chart"> <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" style="height:540px" id="svg-chart"></svg> </div>');
	rowRight.append(svgChart);
	columnRight.append(rowRight);

	$('#profile-content').empty();
	$('#profile-content').append(columnLeft);
	$('#profile-content').append(columnRight);
}

function setDefaultSettings(baselinesActived, labelsActivated, scalingFactors, isXScaleLinear,
	minTau, maxTau, minBaselineMetric, minUnsolvedMetric, data) {

	function setCheckboxes(checkboxes,status) {
		checkboxes.each(function(c){
			if(status[c]) {
				$(this).addClass('active')
			}
			else {
				$(this).removeClass('active')
			}
		});
	}

	clearPlot();
	clearUIElements();

	setUpPlotModel(data);
	let plotModel = PlotManager.getInstance();
	plotModel.isBaselineActivated = baselinesActived;
	plotModel.isLabelActivated = labelsActivated;
	plotModel.scalingFactors = scalingFactors;
	plotModel.isXScaleLinear = isXScaleLinear;
	plotModel.minTau = minTau;
	plotModel.maxTau = maxTau;
	plotModel.minBaselineMetric = minBaselineMetric;
	plotModel.minUnsolvedMetric = minUnsolvedMetric;

	setUpPlotUI();
	generateUIElementsFromData();

	$("#xscale-type-checkbox").bootstrapSwitch('state', isXScaleLinear);

	setCheckboxes(baselines(), baselinesActived);
	setCheckboxes(labels(), labelsActivated);

	components().each(function(f) {
		$(this).slider('setValue', scalingFactors[f]);
	});

	$("#minTau").val(minTau);
	$("#maxTau").val(maxTau);
	$("#minBaselineMetric").val(minBaselineMetric);
	$("#minUnsolvedMetric").val(minUnsolvedMetric);

}
