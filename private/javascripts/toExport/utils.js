/**
 * @author Sascha Van Cauwelaert
 */

function checkData(jsonObject) {
  var result = tv4.validateResult(jsonObject, schema);
  if(!result.valid){
    modalAlert("Errors in JSON file", "There are error(s) in your JSON data file : " + JSON.stringify(result.error,null, 4));
    return false;
  }
  if(tv4.missing.length > 0) {
    modalAlert("Missing schemas", "Missing schemas: " + JSON.stringify(tv4.missing));
    return false;
  }
  if (!functionalChecksOfInputDataPassed(jsonObject)){
    return false;
  }
  return true;
}

//makes a bit more checks that could not be done via the JSON schema
function functionalChecksOfInputDataPassed(inputDataObject) {

    var nLabels = parseInt(inputDataObject.labels.length);
    var data = inputDataObject.data;
    var instanceLabelIndices = inputDataObject.instances;
    var nInstances = instanceLabelIndices.length;

    //check the label indices are in the correct range
    for(var inst=0; inst < instanceLabelIndices.length; inst++) {
      for(var ind=0; ind < instanceLabelIndices[inst].length; ind++) {
        var labelIndex = instanceLabelIndices[inst][ind];
        if(labelIndex < 0 || labelIndex >= nLabels) {
          modalAlert("Error in JSON input", "Error in the instances property of the JSON file. The index " + labelIndex + " is not in the correct range of label indices.");
          return false;
        }
      }
    }

    var dataIsOK = true;
    //check all the metric components of all approaches have the same number of instances
    $.each(data, function(approachKey, approach) {
      $.each(approach, function(componentKey, component) {
        if(dataIsOK && component.length != nInstances){
          modalAlert("Error in JSON input", "Error in the data of the JSON file. The number of values for the component " + componentKey + " of the approach " + approachKey + " is different from the number of arrays in the instances property.");
          dataIsOK = false;
        }
      });
    });

    return dataIsOK;
}

function savePlots() {

	function generateStyleDefs(svgDomElement) {
		var styleDefs = "";
		var sheets = document.styleSheets;
		for (var i = 0; i < sheets.length; i++) {
		  var rules = sheets[i].cssRules;
		  for (var j = 0; j < rules.length; j++) {
			var rule = rules[j];
			if (rule.style) {
			  var selectorText = rule.selectorText;
			  var elems = svgDomElement.querySelectorAll(selectorText);

			  if (elems.length) {
				styleDefs += selectorText + " { " + rule.style.cssText + " }\n";
			  }
			}
		  }
		}

		var s = document.createElement('style');
		s.setAttribute('type', 'text/css');
		s.innerHTML = styleDefs;

		var defs = document.createElement('defs');
		defs.appendChild(s);
		svgDomElement.insertBefore(defs, svgDomElement.firstChild);
	}

	//save svg
  var mathJaxGlyphs = d3.select("#MathJax_SVG_glyphs").node();
  var svg = d3.select("#chart svg");
  svg.append(function(){
    return mathJaxGlyphs;
  });
  var svgNode = svg.node();
	generateStyleDefs(svgNode);
	saveAs(new Blob([svgNode.parentNode.innerHTML], {type:"application/svg+xml"}), "performance-profile.svg");
}




// ------------------------ helper functions ----------------------------

function nbInstances() {
    var data = approaches();
    var firstApproach = data[Object.keys(data)[0]];
    return firstApproach[Object.keys(firstApproach)[0]].length;
}

function approaches() {
  let plotModel = PlotManager.getInstance();
  return plotModel.currentInputData.data;
}

function approachNames() {
    return Object.keys(approaches());
}

//compute the total amount of metric for a given approach
//some of the metric components might be scaled with a factor, if isFictional is set to true
function totalMetricOfApproach(approach, isFictional, componentRatios) {
    var numInstances = nbInstances();
    var totalMetric = newArray(numInstances, 0);
    let components = Object.keys(approach);
    for (var i = 0 ; i < numInstances ; i++) {
      for (var c = 0 ; c < components.length ; c++) {
          let component = components[c];
          if (isFictional)
              totalMetric[i] += approach[component][i] * componentRatios[component];
          else
              totalMetric[i] += approach[component][i];
      }
    }
    return totalMetric;
}


//filter out instances for which the time of one of the selected baselines is smaller than the minimum value (or equals 0 to prevent dividing by 0), or with a non-activated label
function keptInstanceIndices(minimalMetric, activatedLabels, dataLabels, activatedBaselines) {
  let plotModel = PlotManager.getInstance();

  let totalMetricOfApproaches = [];
  var data = plotModel.currentInputData.data;
  for(var i = 0; i < activatedBaselines.length; i++) {
    let b = activatedBaselines[i];
    totalMetricOfApproaches[i] = totalMetricOfApproach(data[b], false, null);
  }

  function isInstanceWithABaselineLargerThanMinimum(index) {
    var isLarger = true;
    for(var i = 0; i < activatedBaselines.length; i++) {
      var totalMetric = totalMetricOfApproaches[i][index];
      if (totalMetric === 0 || totalMetric < minimalMetric) {
        isLarger = false;
      }
    }
    return isLarger;
  }

  var indices = d3.range(0, nbInstances());
  indices = indices.filter(isInstanceWithABaselineLargerThanMinimum);

  let shouldKeepInstance = function(i){
      var isInstanceKept = true;
      var labelIdsOfInstance = plotModel.currentInputData.instances[i];
      for(var l = 0 ; isInstanceKept && l < labelIdsOfInstance.length; l++) {
          var label = dataLabels[labelIdsOfInstance[l]];
          if ($.inArray(label, activatedLabels) < 0){
              isInstanceKept = false;
          }
      }
      return isInstanceKept;
  };
  var kept = indices.filter(shouldKeepInstance);
  return kept;
}

//compute the baseline based on current selected baselines (for each instance, minimum of all )
function baselineForInstances(activatedBaselines, componentRatios) {
    var numInstances = nbInstances();
    var baselineValues = newArray(numInstances, Number.MAX_VALUE);
    var data = approaches();

    activatedBaselines.forEach(function(approachName) {
        var approachValues = totalMetricOfApproach(data[approachName], true, componentRatios);
        for (var i = 0 ; i < numInstances ; i++) {
            baselineValues[i] = Math.min(baselineValues[i], approachValues[i]);
        }
    });
    return baselineValues;
}

function dataOfApproach(approach, approachName, keptIds, activatedBaselines, componentRatios, taus, minUnsolvedMetric) {
    var values = allMetricRatios(approach, keptIds, activatedBaselines,componentRatios, minUnsolvedMetric);
    var profileData = Array(taus.length);
    values.sort();//TODO:do a custom sort should be faster
    let currentNbInstanceSlowerThanTau = 0;
    let nbValues = values.length;
    for (var t = 0; t < taus.length ; t++ ) {
      let tau = taus[t];
      while(values[currentNbInstanceSlowerThanTau] <= tau) {
        currentNbInstanceSlowerThanTau++;
      }
      profileData[t] = {"x" : tau, "y" : currentNbInstanceSlowerThanTau/nbValues * 100};
    }
    return {values: profileData, key: approachName};
}

//if an approach has an instance solved in more than minUnsolvedMetric, it is considered as unsolved, so has a ratio of Number.MAX_VALUE
function allMetricRatios(approach, keptIds, activatedBaselines,componentRatios, minUnsolvedMetric) {
    var baselineValues = baselineForInstances(activatedBaselines, componentRatios);
    var metricValues = totalMetricOfApproach(approach, true, componentRatios);

    function replaceByInfinityIfLarger(i) {
        if (metricValues[i] < minUnsolvedMetric)
            return metricValues[i]/baselineValues[i];
        else
            return Number.MAX_VALUE;
    }
    return keptIds.map(replaceByInfinityIfLarger);
}

//return the maximum ratio (with the current baselines) of the data that is currently considered
//if an approach has an instance solved in more than minUnsolvedMetric, it is considered as unsolved
function largestMetricRatioForSolvedInstances(keptIds, activatedBaselines,componentRatios, minUnsolvedMetric) {
    var largestRatio = Number.MIN_VALUE;
    var appr = approaches();
    approachNames().forEach(function(name) {
        var ratios = allMetricRatios(appr[name], keptIds, activatedBaselines, componentRatios, minUnsolvedMetric);
        var ratiosForSolvedInstances = ratios.filter(function(r) {return r < Number.MAX_VALUE});
        var maxRatioOfApproach = d3.max(ratiosForSolvedInstances);
        largestRatio = Math.max(largestRatio, maxRatioOfApproach);
    })
    return largestRatio;
}


function newArray(size, initValue) {
  let iV = initValue ? initValue : 0
  var arr = [];
  for(var i = 0; i < size; i++){
      arr.push(iV);
  }
  return arr;
}
