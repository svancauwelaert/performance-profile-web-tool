var PlotManager = (function () {

    class Axis {

      constructor(scale, d3Axis, d3CustomAxis, group) {
        this._scale = scale;
        this._d3Axis = d3Axis;
        this._d3CustomAxis = d3CustomAxis;
        this._group = group;
      }

      get scale() {
        return this._scale;
      }

      get d3Axis() {
        return this._d3Axis;
      }

      get d3CustomAxis() {
        return this._d3CustomAxis;
      }

      get group() {
        return this._group;
      }

      set scale(scale) {
        this._scale = scale;
      }

      set d3Axis(d3Axis) {
        this._d3Axis = d3Axis;
      }

      set d3CustomAxis(d3CustomAxis) {
        this._d3CustomAxis = d3CustomAxis;
      }

      set group(group) {
        this._group = group;
      }


    }

    class PlotModel {
      constructor() {
        this.x = new Axis();
        this.y = new Axis();
        this.largestTauForSolvedInstances = 0;
        this.xOffset =  100;
        this.plotWidth = d3.select("#chart svg").node().getBoundingClientRect().width - 20;
        this.svgWidth = d3.select("#chart svg").node().getBoundingClientRect().width;
        this.profile = null;
        this.colorScale = null;
        // this.numTau = 100;
        this.numTau = 500;
        // this.numTau = 5000;
        this.tauMin = minTau();
        this.tauMax = maxTau();
        this.isXScaleLinear = isXScaleLinear();
        this.minBaselineMetric = minBaselineMetric();
        this.minUnsolvedMetric = minUnsolvedMetric();
        this.currentInputData = null;
        this.metricName = null;
        this.labels = null;
        this.keptIds = null;
      }

      dataProfiles() {
          this.updateKeptInstances();
          let taus = null;

          if(this.isXScaleLinear) {
            let stepSizeBeforeTauMax = (this.tauMax - this.tauMin) / (this.numTau/2);
            let tausBeforeTauMax = d3.range(this.tauMin,this.tauMax, stepSizeBeforeTauMax);
            if(tausBeforeTauMax[tausBeforeTauMax.length - 1] < this.tauMax)
              tausBeforeTauMax.push(this.tauMax);
            let stepSizeAfterTauMax = (this.largestTauForSolvedInstances - this.tauMax) / (this.numTau/2);
            let tausAfterTauMax = d3.range(this.tauMax,this.largestTauForSolvedInstances, stepSizeAfterTauMax);
            taus = tausBeforeTauMax.concat(tausAfterTauMax);
          }
          else {
            let stepSize = (this.tauMax - this.tauMin) / this.numTau;
            let d3XScale = d3.scalePow(2).domain([this.tauMin,this.tauMax]).range([this.tauMin,this.tauMax]);
            taus = d3.range(this.tauMin, this.tauMax, stepSize).map(d3XScale); // the tau values grows exponentially to have a smoother line when the x scale is in log
          }

          var data = [];
          var approachesData = approaches();

          let that = this;
          let names = approachNames();
          for(var n = 0; n < names.length; n ++) {
            let name = names[n];
            data.push(dataOfApproach(approachesData[name], name, that.keptIds, that.activatedBaselines(), that.componentRatios(), taus, that.minUnsolvedMetric));
          }

          return data;
      }

      updateKeptInstances () {
        this.keptIds = keptInstanceIndices(this.minBaselineMetric, this.activatedLabels(), this.labels, this.activatedBaselines());
        this.largestTauForSolvedInstances = largestMetricRatioForSolvedInstances(this.keptIds, this.activatedBaselines(), this.componentRatios(),
        this.minUnsolvedMetric);
      }

      baselines() {
        return Object.keys(this.currentInputData.data);
      }

      activatedBaselines() {
        let that = this;
        return Object.keys(this.currentInputData.data).filter( function(n,i) {return that.isBaselineActivated[i]});
      }

      activatedLabels() {
        let that = this;
        return this.labels.filter( function(n,i) {return that.isLabelActivated[i]});
      }

      metricComponents() {
        var components = [];
        Object.values(this.currentInputData.data).forEach(function (approach) {
          Object.keys(approach).forEach(function (componentName) {
            if (components.indexOf(componentName) < 0)
              components.push(componentName);
          });
        });

        return components;
      }

      componentRatios() {
         let components = this.metricComponents();
         let ratios = {};
         for (let i = 0 ; i < components.length ; i++) {
            ratios[components[i]] = this.scalingFactors[i];
         }
         return ratios;
      }



    }

    var plot;

    function createInstance() {
        var object = new PlotModel();
        return object;
    }

    return {
        getInstance: function () {
            if (!plot) {
              plot = createInstance();
            }
            return plot;
        },

        delete: function () {
            plot = undefined;
        }
    };

})();
