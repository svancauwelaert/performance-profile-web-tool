function mathjax() {

   var svg = d3.select('#svg-chart');
   let plotModel = PlotManager.getInstance();

   MathJax.Hub.Config({
     extensions: ["tex2jax.js"],
     tex2jax: {
       inlineMath: [ ['$','$'], ["\\(","\\)"] ],
       processEscapes: true
     }
   });

   MathJax.Hub.Register.MessageHook("New Math", callBackOnceMathJaxIsDone);
   MathJax.Hub.Queue(["Typeset", MathJax.Hub, svg.node()]);
}

function callBackOnceMathJaxIsDone() {

  let svg = d3.select('#svg-chart');
  let plotModel = PlotManager.getInstance();
  let mathjaxElemStillToRender = false;
  //tau
  var tau = svg.select('#tau');
  var tauSvg = d3.select('#tau>span>svg');
  tauSvg
  .attr("x", (plotModel.xOffset + plotModel.svgWidth)/2)
  .attr("y", 527);

  if(!tauSvg.empty()) {
    svg.append(function(){
     return tauSvg.node();
    });

    var tauWidth = tauSvg.node().getBoundingClientRect().width;
    var tauHeight = tauSvg.node().getBoundingClientRect().height;
    tauSvg
    .attr("x", tauSvg.attr('x') - tauWidth/2)
    .attr("y", tauSvg.attr('y') - tauHeight/2)

    tau.remove();
  }

  //instances
  var instances = svg.select('#instances');
  var instancesSvg = svg.select('#instances>span>svg');

  if(!instancesSvg.empty()) {
    svg.append('g')
    .attr('transform', "rotate(-90) " + "translate(" + -(560 / 2) + ","+ 3*plotModel.xOffset/4 + ") ")
    .append(function(){
     return instancesSvg.node();
  });

  var instancesWidth = instancesSvg.node().getBoundingClientRect().width;
  var instancesHeight = instancesSvg.node().getBoundingClientRect().height;
  instancesSvg
  .attr("x", instancesSvg.attr('x') - instancesWidth/2)
  .attr("y", instancesSvg.attr('y') - instancesHeight/2)

  instances.remove();
  }

  //mathjax elements
  svg.selectAll('.mathjaxElem').each(function(){
    var self = d3.select(this),
        mathSvg = self.select('text>span>svg');
    var parentNode = d3.select(this.parentNode);

    if(!mathSvg.empty()) {
      parentNode.append(function(){
        return mathSvg.node();
      });

      //center the math elements correctly
      var width = mathSvg.node().getBoundingClientRect().width;
      var height = mathSvg.node().getBoundingClientRect().height;
      mathSvg.attr("x", 40 - width/2);
      mathSvg.attr("y", -height/2);

      self.remove();
    }
    else {
      mathjaxElemStillToRender = true;
    }
  });

  //put the labels in the correct position
  if(!mathjaxElemStillToRender) {
    positionLabels();
  }


}
