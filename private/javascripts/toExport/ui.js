/**
 * @author Sascha Van Cauwelaert
 */

var sliderPrefix = "slider-";

$(function(){

   $("#minTau" ).change(function() {
      let plotModel = PlotManager.getInstance();
      plotModel.tauMin = minTau();
      updateXAxis();
      updateProfiles();
   });

   $("#maxTau" ).change(function() {
     let plotModel = PlotManager.getInstance();
     plotModel.tauMax = maxTau();
     updateXAxis();
     updateProfiles();
   });

   $("#minBaselineMetric" ).change(function() {
      let plotModel = PlotManager.getInstance();
      plotModel.minBaselineMetric = minBaselineMetric();
      plotModel.updateKeptInstances();
      updateXAxis();
      updateProfiles();
   });

   $("#minUnsolvedMetric" ).change(function() {
      let plotModel = PlotManager.getInstance();
      plotModel.minUnsolvedMetric = minUnsolvedMetric();
      plotModel.updateKeptInstances();
      updateXAxis();
      updateProfiles();
   });

   $("#xscale-type-checkbox").on('switchChange.bootstrapSwitch', function () {
     let plotModel = PlotManager.getInstance();
     plotModel.isXScaleLinear = isXScaleLinear();
     if(!plotModel.isXScaleLinear && plotModel.tauMin < 1) {
       plotModel.tauMin = 1;
       $("#minTau").val(1);
     }
     updateXAxis();
     updateProfiles();
   });
});

function loadDataFile() {

  var f = document.getElementById("jsonInputFile").files[0];
  var r = new FileReader();
  r.onload = function(e) {
      var contents = e.target.result;
      try {
        let newData = JSON.parse(contents);
        try {
          //only redraw if the input data is correct according to schema and functional checks
          if(checkData(newData)){
            clearPlot();
            clearUIElements();
            setUpPlotModel(newData);
            generateUIElementsFromData();
            setUpPlotUI();
            redraw();
          }
        } catch(e) {
          modalAlert("Error while plotting from JSON data file", e);
        }
      } catch(e) {
        modalAlert("Error while parsing JSON file", e);
      }
  };
  r.onerror = function (evt) {
      console.log("Error while reading file.");
  };

  if (f) {
    r.readAsText(f);
  }
}


function generateUIElementsFromData() {

  let plotModel = PlotManager.getInstance();

  function changeActiveStatus(item) {
      if (item.hasClass('active'))
          item.removeClass('active');
      else
          item.addClass('active');
  }

  //baselines
  $('#baselines').append('<li role="separator" class="divider"></li>');

  Object.keys(plotModel.currentInputData.data).forEach(function (key, i) {
    var baselineCheckboxId = "baseline-checkbox-" + key.split('\\').join('_');
    $('#baselines').append('<li name="' + key + '" id="' + baselineCheckboxId + '" class="active"><a>'+key+'</a></li>');
    $('#baselines').append('<li role="separator" class="divider"></li>');
    $("[id='"+baselineCheckboxId+"']").children().first().on("click",function() {
                                                       changeActiveStatus($(this).parent());
                                                       if(activatedBaselines().length == 0)
                                                          baselines().addClass('active');
                                                       plotModel.isBaselineActivated[i] = !plotModel.isBaselineActivated[i];
                                                       plotModel.updateKeptInstances();
                                                       updateXAxis();
                                                       updateProfiles();
                                                     });
  });

  //labels
  $('#labels').append('<li role="separator" class="divider"></li>');
  plotModel.currentInputData.labels.forEach(function (labelName, i) {
    var labelCheckboxId = "label-checkbox-" + labelName.split('\\').join('_');;
    $('#labels').append('<li name="' + labelName + '" id="' + labelCheckboxId + '" class="active"><a>'+labelName+'</a></li>');
    $('#labels').append('<li role="separator" class="divider"></li>');
    $("[id='"+labelCheckboxId+"']").children().first().on("click",function() {
                                                       changeActiveStatus($(this).parent());
                                                       plotModel.isLabelActivated[i] = !plotModel.isLabelActivated[i];
                                                       plotModel.updateKeptInstances();
                                                       updateXAxis();
                                                       updateProfiles();
                                                     });
  });

  //metric components
  plotModel.metricComponents().forEach(function(componentName,i) {
    var sliderDivId=sliderPrefix+componentName;
    var sliderDiv = $('<div></div>', {
      id: sliderDivId,
      name: componentName,
      style: "width:100%"
    });
    $('#reduction-sliders').append(sliderDiv);
    sliderDiv.slider({min : 0, max : 1, step: 0.01, value : 1});
    $('#reduction-sliders').append('<label for='+sliderDivId+'>'+componentName+'</label>');
    sliderDiv.on('change', function(slideEvt) {
        plotModel.scalingFactors[i] = slideEvt.value.newValue;
        plotModel.updateKeptInstances();
        updateXAxis();
        updateProfiles();
    });

  });
}

/*
 * Helper UI functions
 */

 function clearUIElements() {
   $('#baselines').empty();
   $('#labels').empty();
   $('#reduction-sliders').empty();
 }

function baselines() {
  return $('#baselines').find("li").filter("li[id^='baseline']");
}

function labels() {
  return $('#labels').find("li").filter("li[id^='label']");;
}

function inputDataButtons() {
  return $('#input-buttons').children();
}

function deactivateAllDataButtons() {
  inputDataButtons().each(function(index) {
    $(this).removeClass('active');
  });
}

function clearChart() {
  d3.select('#chart svg').remove();

  d3.select('#chart').append("svg")
  .attr("height", 540)
  .attr('xmlns','http://www.w3.org/2000/svg')
}

// function plotInitialData() {
//   let plotModel = PlotManager.getInstance();
//   setUpPlotModel(plotModel.currentInputData);
//   setUpPlotUI();
//   plotProfiles();
// }

function activatedBaselines() {
   return activatedElementsOfSelection(baselines());
}

function activatedLabels() {
   return activatedElementsOfSelection(labels());
}

function activatedElementsOfSelection(jQuerySelection) {
   return jQuerySelection.filter(function() {return $(this).hasClass("active");}).map(function() {return $(this).attr("name")});
}

function components() {
  return $("div[id^='" + sliderPrefix +"']");
}

function isXScaleLinear() {
  return $("#xscale-type-checkbox").is(":checked");
}

function minTau() {
  return Number(document.getElementById("minTau").value);
}

function maxTau() {
  return Number(document.getElementById("maxTau").value);
}

function minBaselineMetric() {
  return Number(document.getElementById("minBaselineMetric").value);
}
function minUnsolvedMetric() {
  return Number(document.getElementById("minUnsolvedMetric").value);
}

function modalAlert(title, msg) {
  $("#modelTitle").text(title);
  $("#modelContent").text(msg);
  $("#profile-modal").modal();
}
