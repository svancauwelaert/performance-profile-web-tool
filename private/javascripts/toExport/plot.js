/**
 * @author Sascha Van Cauwelaert
 */

 function setUpPlotModel(jsonObject) {
   let plotModel = PlotManager.getInstance();

   plotModel.currentInputData = jsonObject;
   plotModel.metricName = jsonObject.metric;
   plotModel.labels = jsonObject.labels;
   plotModel.isLabelActivated = plotModel.labels.map(function() {return true;}); //by default they are all activated
   plotModel.isBaselineActivated = approachNames().map(function() {return true;});
   plotModel.scalingFactors = plotModel.metricComponents().map(function() {return 1;});
   plotModel.colorScale = d3.scaleOrdinal(d3.schemeCategory10).domain(Object.keys(plotModel.currentInputData.data));

   plotModel.isProfileVisible = {};
   Object.keys(plotModel.currentInputData.data).forEach(key => plotModel.isProfileVisible[key] = true);
 }

function setUpPlotUI() {
  drawGraphCanevas();
  let plotModel = PlotManager.getInstance();
  let data = plotModel.dataProfiles();
  drawLabels(data);
  mathjax();
}

function plotProfiles() {
  let plotModel = PlotManager.getInstance();
  let tauMin=plotModel.minTau, tauMax=plotModel.maxTau;
  let metricName = plotModel.metric;
  if (tauMax <= tauMin) {
      return;
  }

  let data = plotModel.dataProfiles();
  drawProfiles(data);
}

function drawGraphCanevas() {
  let svg = d3.select("#svg-chart");

  let minY = 35;
  let maxY = 490;
  let plotModel = PlotManager.getInstance();
  plotModel.updateKeptInstances();

  plotModel.x.scale = d3.scaleLinear().domain([plotModel.tauMin, plotModel.tauMax, plotModel.largestTauForSolvedInstances]).range(
    [plotModel.xOffset,
      plotModel.plotWidth - plotModel.xOffset
      ,
      plotModel.plotWidth
    ]);
  plotModel.y.scale = d3.scaleLinear().domain([0,100]).range([maxY, minY]);

  let range = d3.range(plotModel.tauMin, plotModel.tauMax, (plotModel.tauMax - plotModel.tauMin)/10);
  range.push(plotModel.tauMax);
  range.push(plotModel.largestTauForSolvedInstances);

  plotModel.x.d3Axis = d3.axisBottom(plotModel.x.scale).tickFormat(d3.format(',.2r')).tickValues(range);
  plotModel.y.d3Axis = d3.axisLeft(plotModel.y.scale).tickFormat(d3.format('.1r'));

  plotModel.x.d3CustomAxis = function (g) {
    g.call(plotModel.x.d3Axis);
    g.selectAll(".tick:not(:first-of-type) line").attr("y1", 0)
    .attr("y2", -(maxY - minY)).attr("stroke", "#e5e5e5")

    g.selectAll(".tick text").attr("font-family", "Arial")
    .attr("font-size", "18px")
    .attr("font-weight", "normal")
  };

  plotModel.y.d3CustomAxis = function (g) {
    g.call(plotModel.y.d3Axis);
    g.selectAll(".tick:not(:first-of-type) line").attr("x1", 0)
    .attr("x2", plotModel.plotWidth - plotModel.xOffset).attr("stroke", "#e5e5e5")

    g.selectAll(".tick text").attr("font-family", "Arial")
    .attr("font-size", "18px")
    .attr("font-weight", "normal")
  }

  plotModel.x.group = svg.append("g")
      .attr("transform", "translate(" + 0 +"," + maxY + ")")
      .call(plotModel.x.d3CustomAxis);

  plotModel.y.group = svg.append("g")
      .attr("transform", "translate(" + plotModel.xOffset +"," + 0 + ")")
      .call(plotModel.y.d3CustomAxis);

  // text label for the x axis
  svg.append("text")
      .style("text-anchor", "middle")
      .attr("id","tau")
      .text("$\\large \\tau \\text{ }" + "( \\mathit{" + plotModel.metricName + "} )$");

  // text label for the y axis
  svg.append("text")
      .attr("id","instances")
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("$\\large \\% \\text{ } \\mathit{ instance}$");
}



function drawLabels(data) {
  let svg = d3.select('#svg-chart');
  let plotModel = PlotManager.getInstance();

  var approachGroups = svg.selectAll('approaches')
  .data(data.reverse())
  .enter()
  .append('g')
  .attr('class','approaches')
  ;

  approachGroups
  .append('circle')
  .attr("r", 5)
  .style("fill", d => plotModel.colorScale(d.key))
  .style("stroke", d => plotModel.colorScale(d.key))
  .on("click", function(d){
    plotModel.isProfileVisible[d.key] = !plotModel.isProfileVisible[d.key];
    d3.select(this).style("fill", d => plotModel.isProfileVisible[d.key] ? plotModel.colorScale(d.key) : 'white')
    updateProfiles();
  })
  ;

  approachGroups
  .append('text')
  .text(d => "$" + d.key + "$")
  .attr("x", 20)
  .attr("font-family", "Arial")
  .attr("font-size", "18px")
  .style("dominant-baseline", "middle")
  .attr('class', "mathjaxElem")
  ;

}

function drawProfiles(data) {
  let svg = d3.select("#svg-chart");
  let plotModel = PlotManager.getInstance();

  function xScale(d) {
    return plotModel.x.scale(d.x);
  }
  function yScale(d) {
    return plotModel.y.scale(d.y);
  }
  function valuesForProfile(d) {
    return plotModel.profile(d.values);
  }

  plotModel.profile = d3.line()
  .curve(d3.curveStepAfter)
  .x(xScale)
  .y(yScale);

  svg.selectAll('profiles')
      .data(data)
      .enter()
      .append("path")
      .attr('class','profiles')
      .attr("d", valuesForProfile)
      .style('fill','none')
      .style('stroke',d => plotModel.colorScale(d.key))
      .style('stroke-width',2)
      ;
}

function updateXAxis() {

  let plotModel = PlotManager.getInstance();
  let range = d3.range(plotModel.tauMin,plotModel.tauMax, (plotModel.tauMax - plotModel.tauMin)/10);
  range.push(plotModel.tauMax);
  range.push(plotModel.largestTauForSolvedInstances);

  if(plotModel.isXScaleLinear) {
    plotModel.x.scale = d3.scaleLinear().domain([plotModel.tauMin, plotModel.tauMax, plotModel.largestTauForSolvedInstances]).range([plotModel.xOffset, plotModel.plotWidth - plotModel.xOffset ,plotModel.plotWidth]);
    plotModel.x.d3Axis = d3.axisBottom(plotModel.x.scale).tickFormat(d3.format(',.2r')).tickValues(range);
  }
  else {
    plotModel.x.scale = d3.scaleLog().base(2).domain([plotModel.tauMin, plotModel.tauMax]).range([plotModel.xOffset, plotModel.plotWidth]);
    plotModel.x.d3Axis = d3.axisBottom(plotModel.x.scale).tickFormat(d3.format(',.2r'));
  }

  if(plotModel.x.group) {
    plotModel.x.group
    .transition()
    .duration(500)
    .call(plotModel.x.d3CustomAxis);
  }

}

function updateProfiles() {
  let plotModel = PlotManager.getInstance();
  let newData = plotModel.dataProfiles();

  function valuesForProfile(d) {
    return plotModel.profile(d.values);
  }

  d3.selectAll('.profiles')
      .data(newData)
      .transition()
      .duration(500)
      .attr("d", valuesForProfile)
      .style("opacity", d => plotModel.isProfileVisible[d.key] ? 1 : 0)
      ;
}

function clearPlot() {
  PlotManager.delete();
  d3.select("#svg-chart").remove();
  d3.select("#chart").append('svg')
  .style("height","540px").attr('id', 'svg-chart')
  .attr('xmlns', 'http://www.w3.org/2000/svg')
  .attr('version','1.1')
  .attr('xmlns:xlink','http://www.w3.org/1999/xlink')
  ;
}

function redraw() {
  updateXAxis();
  plotProfiles();
}

function positionLabels() {
  let plotModel = PlotManager.getInstance();
  let svg = d3.select('#svg-chart');
  let currentXOffset = plotModel.svgWidth - plotModel.xOffset/2;
  svg.selectAll('.approaches').each(function(g,i) {
    let self = d3.select(this);

    let svgmath = self.select('svg');
    let svgmathWidth = svgmath.node().getBoundingClientRect().width;
    currentXOffset -= svgmathWidth;

    let circle = self.select('circle');
    let circleWidth = circle.node().getBoundingClientRect().width;

    svgmath.attr('x', circleWidth);
    svgmath.attr('y', -circleWidth);
    self.attr('transform', "translate(" + currentXOffset +",15)");
    currentXOffset -= (circleWidth + 30);
  });
}
