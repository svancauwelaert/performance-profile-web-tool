var mergeCollectFiles = require('node-merge-collect-files');
var concat = require('concat-files');
var fs = require('fs');


 concatAndUglify('private/javascripts/toExport', "public/javascripts/performance-profile.min.js", '.js');

 concatAndUglify('private/stylesheets', "public/stylesheets/performance-profile.min.css", '.css');

 function concatAndUglify(inputDirURI, outputFileURI, extension) {
 	var files = mergeCollectFiles( [ inputDirURI ], '**/*'.concat(extension) );

	var filesNames = Object.keys(files).map(function(f) {
		var absolutePath = files[f].absolutePath;
	   return absolutePath;
	});

	if(extension == '.js') {
		var tmpMergedFileURI = "tmpFile".concat(extension);

		concat(filesNames,tmpMergedFileURI, function() {
	    	console.log('done concat');
	    	// UglifyJS = require("uglify-js");
        UglifyJS = require("uglify-es");
        console.log(tmpMergedFileURI);

        //TODO: read content of tmpMergedFileURI and minify the content

        fs.readFile(tmpMergedFileURI, 'utf8', function (err,data) {
          if (err) {
            return console.log(err);
          }
          var result = UglifyJS.minify(data);

    			writeToFile(outputFileURI,result.code);
    			fs.unlink(tmpMergedFileURI, function(err) {
    		   	if (err) {
    		       return console.error(err);
    		   	}
    		   	console.log("File deleted successfully!");
    		  });
        });
	  });
	}
	else if(extension == '.css') {
		var uglifycss = require('uglifycss');
		var result = uglifycss.processFiles(filesNames);
		writeToFile(outputFileURI,result);
	}
	else {
		console.log("problem with extension");
		return;
	}

	function writeToFile(outputFileURI, contentToWrite) {
		fs.writeFile(outputFileURI, contentToWrite, function(err) {
		if(err) {
			return console.log(err);
		}
		console.log("The file was saved!");
	});
	}
 }
