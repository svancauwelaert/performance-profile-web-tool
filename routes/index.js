var express = require('express');
var router = express.Router();

/* GET home page. */
var pageTitle = 'Performance Profiles'
router.get('/', function(req, res, next) {
  res.render('index', { title: pageTitle });
});

router.get('/index.html', function(req, res, next) {
  res.render('index', { title: pageTitle });
});

module.exports = router;
