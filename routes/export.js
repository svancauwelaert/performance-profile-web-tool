var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();

router.post('/', function(req, res, next) {

  var serverURL = req.get('origin');
  var body = req.body;

  var jadeParams = {'data' : JSON.parse(body.data),
  					'minTau' : Number(body.minTau),
  					'maxTau' : Number(body.maxTau),
  					'baselinesActived' : JSON.parse(body.baselinesActived),
                  	'labelsActivated' : JSON.parse(body.labelsActivated),
                  	'scalingFactors' : JSON.parse(body.scalingFactors),
                  	'isXScaleLinear' : JSON.parse(body.isXScaleLinear),
                  	'minBaselineMetric' : Number(body.minBaselineMetric),
                  	'minUnsolvedMetric' : Number(body.minUnsolvedMetric),
                    'pretty' : true,
                    'serverURL' : serverURL
  };

  req.app.render('export', jadeParams , function(err, html){
  	res.setHeader('Content-disposition', 'attachment; filename=export.html');
  	res.setHeader('Content-type', 'text/plain');
  	res.charset = 'UTF-8';

	res.send(html);
	});
});

module.exports = router;
