function exportProfile() {

  let plotModel = PlotManager.getInstance();

  function statusOfActivableElements(elems) {
      return elems.map(function() {return $(this).hasClass("active")}).toArray();
  }

  var baselinesActived = statusOfActivableElements(baselines());
  var labelsActivated = statusOfActivableElements(labels());
  var scalingFactors = plotModel.scalingFactors;

  var toSend = {'data' : JSON.stringify(plotModel.currentInputData),
                    'minTau' : minTau(),
                    'maxTau' : maxTau(),
                    'baselinesActived' : JSON.stringify(baselinesActived),
                    'labelsActivated' : JSON.stringify(labelsActivated),
                    'scalingFactors' : JSON.stringify(scalingFactors),
                    'isXScaleLinear' : JSON.stringify(isXScaleLinear()),
                    'minBaselineMetric' : minBaselineMetric(),
                    'minUnsolvedMetric' : minUnsolvedMetric()
  };

  post("/export/", toSend);

  // Post to the provided URL with the specified parameters.
  function post(path, parameters) {
    var form = $('<form id="hidden-form"></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(parameters, function(key, value) {
        var field = $('<input></input>');

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });

    //temporary hidden iframe to be the target of the hidden form so that the page is not reloaded
    var iframe = $('<iframe style="display:none" id="hidden-iframe" name="hidden-iframe"></iframe>');
    form.attr("target", iframe.attr('name'));

    $(document.body).append(iframe);

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);

    form.submit();

  }
}
