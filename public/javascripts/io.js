$(function() {

	$('#tool-columnRight').append('\
		<hr> \
		<div class="row"> \
                    <div class="col-md-3 col-md-offset-4"> \
                        <input type="file" name="jsonInputFile" id="jsonInputFile" accept=".json" data-show-remove="false" data-show-preview="false" data-show-upload="false" multiple=false data-show-caption="true" class="file-loading"/>\
                    </div>\
                    <div class="col-md-2">\
                        <button type="button" class="btn btn-default" onclick="savePlots()">\
                            <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Download\
                        </button>\
                    </div>\
                    <div class="col-md-2">\
                        <button type="button" class="btn btn-default" onclick="exportProfile()">\
                            <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export as a Web Page\
                        </button>\
                    </div>\
                </div>\
                ')
});