var inputExample = {
  "metric" : "time",
  "labels" : ["Road","Wood"],
  "instances" :  [[0],[0],[0,1],[0,1],[1],[1]],
  "data": {
      "\\mathit{Car A}": {
        "wheels" : [100,30,40,50,10,20],
        "motor" : [20,3,4,5,1,2]
      },
      "\\mathit{Car B}": {
        "wheels" : [10,7,45,55,30,50]
      },
      "\\mathit{Car C}" : {
        "wheels" : [15,12,35,40,15,25],
        "motor" : [10,3,4,5,1,2]
      }
  }
};
