/**
 * @author Sascha Van Cauwelaert
 */

$(function() {
	setDefaultSettings([true, true, true], [true, true], [1,1], true, 0, 2, 0, Number.MAX_SAFE_INTEGER, inputExample);

	plotProfiles();
	// plotInitialData();
	//prepare the link to download the JSON schema
   var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(schema));
   $('#downloadSchema').attr("href", "data:"+data);
   $('#downloadSchema').attr("download", "schema.json");

   //input json file
   $("#jsonInputFile").fileinput('refresh', {browseLabel: 'Data'});
   $("#jsonInputFile").on('fileloaded', function() {
    deactivateAllDataButtons();
    loadDataFile();
   });

   $("#jsonInputFile").fileinput({
                    autoReplace: true,
                    maxFileCount: 1,
                    allowedFileExtensions: ["json"]
                });

   $("#input-example").text(stringify(inputExample));
});
